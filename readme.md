##Projeto

Avaliação técnica - Automação de uma aplicação e-commerce.

##Construção do projeto

Java 8

JUnit

Eclipse IDE for Java Developers - Carbon

Selenium Webdriver

Maven

Sistema Operacional: Windows

Cucumber

Java Faker

Git

## Pré-requisito

Acesse o repositório no **Bitbucket** e ralize o download.
Se preferir clonar o projeto, indico o site de ajuda do Bitbucket para leitura.

##Execução

Para executar o script via linha de comando, digite **mvn -Dtest=TestRunner test**
Uma outra opção é importar o projeto, do diretório selecionado no **Pré-requisito**, para o package explorer no Eclipse.
Clique no projeto ecommerce com o botão direito e na opção **Run As / Maven test**


##Autor

Gustavo de Oliveira Govoni

##Licença

Projeto para avaliação técnica.



