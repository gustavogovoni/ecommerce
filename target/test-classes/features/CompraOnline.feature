#language:pt

@1ºFuncionalidade
 Funcionalidade: Realizar compra
 
 	Como um cliente
	Eu quero escolher um produto qualquer na loja
	Para que eu possa realizar uma compra no site
 
 Contexto:
 	Dado que estou na loja
 
 @1ºCenário
 Cenário: Realizar uma compra com sucesso.
	Quando escolho um produto qualquer 
	
	E  adiciono o produto escolhido ao carrinho
	
	E prossigo para o checkout
	
	E valido se o produto foi corretamente adicionado ao carrinho
	 
	E prossigo caso esteja tudo certo
	
	E realizo o cadastro do cliente preenchendo todos os campos obrigatorios dos formularios
	
	E  valido se o endereço esta correto e prossigo
	
	E aceito os termos de serviço e prossigo
	
	E valido o valor total da compra
	
	E seleciono um método de pagamento e prossigo
	
	Então confirmo a compra e valido se foi finalizada com sucesso
 				 
 				
 				
 