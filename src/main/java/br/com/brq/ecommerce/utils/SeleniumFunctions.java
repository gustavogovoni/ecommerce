package br.com.brq.ecommerce.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

/**
 * @see Classe responsável pela configuração dos métodos do Selenium.
 * @since 15/06/2019
 * @author Gustavo Govoni
 *
 */
public class SeleniumFunctions extends ConfiguiracaoAmbiente {

	public void clicarBotao(String xpath) {
		getDriver().findElement(By.xpath(xpath)).click();
	}

	public void scrollObj(String xpath) {
		try {
			WebElement elemento = getDriver().findElement(By.xpath(xpath));
			Actions actions = new Actions(getDriver());
			actions.moveToElement(elemento).perform();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String obterTexto(String xpath) {

		return getDriver().findElement(By.xpath(xpath)).getText();
	}

	public void escreverTexto(String xpath, String texto) {
		getDriver().findElement(By.xpath(xpath)).sendKeys(texto);
	}

	public void selecionarCombo(String xpath, String option) {
		WebElement element = getDriver().findElement(By.xpath(xpath));
		Select combo = new Select(element);
		combo.selectByVisibleText(option);

	}

}
