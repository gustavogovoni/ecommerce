package br.com.brq.ecommerce.utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * @see Classe responsável pela configuração do browser e print de evidência.
 * @since 15/06/2019
 * @author Gustavo Govoni
 *
 */

public class ConfiguiracaoAmbiente {

	protected static WebDriver driver;
	private String path = System.getProperty("user.dir");
	protected ConfiguiracaoAmbiente() {
	}

	public static WebDriver getDriver() {

		if (driver == null) {
			
			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized");
			options.addArguments("disable-infobars");
			driver = new ChromeDriver(options);
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
		}
		return driver;
	}

	public static void iniciaUrl(String url) {
		getDriver().get(url);

	}

	public static void quitDriver() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}
	
	
	String rootOrigem = null;
	String rootDestino = null;
	LocalDateTime agora = LocalDateTime.now();
	DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss");
	
	public void screenShot(String fileName) throws IOException{
		
		rootOrigem = this.path + "\\screenshot\\";
		rootDestino = rootOrigem + fileName + "_" + agora.format(formato) + ".jpg";
		TakesScreenshot ss = (TakesScreenshot) getDriver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo,new File(rootDestino));
		System.out.println("Caminho do print: " + rootOrigem);
	}
	
}
