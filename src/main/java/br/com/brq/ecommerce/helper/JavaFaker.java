package br.com.brq.ecommerce.helper;

import com.github.javafaker.Faker;

public class JavaFaker {

	Faker faker = new Faker();

	public String criarEmail() {
		String email = "";
		// Faker faker = new Faker();
		email = faker.internet().emailAddress();
		return email;
	}

	public String criarNome() {
		String nome = "";
		// Faker faker = new Faker();
		nome = faker.name().firstName();
		return nome;
	}

	public String criarSobrenome() {
		String sobreNome = "";
		sobreNome = faker.name().lastName();
		return sobreNome;

	}

	public String criarEndereco() {
		String endereco = "";
		endereco = faker.address().streetAddress();
		return endereco;
	}

}
