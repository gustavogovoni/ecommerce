package br.com.brq.ecommerce.helper;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.brq.ecommerce.utils.ConfiguiracaoAmbiente;

public class Esperas extends ConfiguiracaoAmbiente{
//Wait<WebDriver> wait;

	public void esperaFixa() throws InterruptedException{
		Thread.sleep(10000);
			
	}
	 public void esperaImplicita(){
		 getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	 }
	
	 public void esperaExplicita(List<WebElement> elements){		 
		 WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		 wait.until(ExpectedConditions.visibilityOfAllElements(elements));
	 }
	 
}
