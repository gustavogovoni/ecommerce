package br.com.brq.ecommerce.steps;

import java.io.IOException;

import org.junit.Assert;

import br.com.brq.ecommerce.helper.Esperas;
import br.com.brq.ecommerce.pages.CarrinhoPage;
import br.com.brq.ecommerce.pages.ContaPage;
import br.com.brq.ecommerce.pages.EnderecoPage;
import br.com.brq.ecommerce.pages.HomePage;
import br.com.brq.ecommerce.pages.PagamentoPage;
import br.com.brq.ecommerce.pages.RemessaPage;
import br.com.brq.ecommerce.pages.ResumoPage;
import br.com.brq.ecommerce.utils.ConfiguiracaoAmbiente;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

/**
 * @see Classe responsável pela configuração dos passos do teste.
 * @since 15/06/2019
 * @author Gustavo Govoni
 *
 */

public class CompraOnlineSteps extends ConfiguiracaoAmbiente {
	HomePage page;
	Esperas wait;
	CarrinhoPage carrinho;
	ResumoPage produto;
	ContaPage conta;
	EnderecoPage set;
	RemessaPage entrega;
	PagamentoPage mostra;

	@Before
	public void inicializa() {
		getDriver();
		page = new HomePage();
		wait = new Esperas();
		carrinho = new CarrinhoPage();
		produto = new ResumoPage();
		conta = new ContaPage();
		set = new EnderecoPage();
		entrega = new RemessaPage();
		mostra = new PagamentoPage();
	}

	@Dado("^que estou na loja$")
	public void queEstouNaLoja() throws Throwable {
		iniciaUrl("http://automationpractice.com");
	}

	@Quando("^escolho um produto qualquer$")
	public void escolhoUmProdutoQualquer() throws Throwable {
		page.selecionarProduto("//*[@id='homefeatured']//div[@class='right-block']/h5/a[@title='Blouse']");
	}

	@E("^adiciono o produto escolhido ao carrinho$")
	public void adicionoOProdutoEscolhidoAoCarrinho() throws Throwable {
		carrinho.adicionarCarrinho();
	}

	@E("^prossigo para o checkout$")
	public void prossigoParaOCheckout() throws Throwable {
		carrinho.prosseguirCheckout();
	}

	@E("^valido se o produto foi corretamente adicionado ao carrinho$")
	public void validoSeOProdutoFoiCorretamenteAdicionadoAoCarrinho() throws Throwable {
		Assert.assertEquals("Blouse", produto.validarProdutoCarrinho());
	}

	@E("^prossigo caso esteja tudo certo$")
	public void prossigoCasoEstejaTudoCerto() throws Throwable {
		produto.prosseguirCheckout();
	}

	@E("^realizo o cadastro do cliente preenchendo todos os campos obrigatorios dos formularios$")
	public void realizoOCadastroDoClientePreenchendoTodosOsCamposObrigatoriosDosFormularios() throws Throwable {
		
		conta.inserirEmail(false);
		conta.clicarCriarConta();

		set.insereNome(false);
		set.insereSobreNome(false);
		set.insereEmail();
		set.insereSenha();
		set.insereEndereco();
		set.insereCidade();
		set.selecionaEstado();
		set.insereCodPostal();
		set.selecionaPais();
		set.insereCelular();
		set.insereReferencia();
		set.clicarRegistrar();

	}

	@E("^valido se o endereço esta correto e prossigo$")
	public void validoSeOEndereçoEstaCorretoEProssigo() throws Throwable {
		Assert.assertEquals("Chicago, Alabama 00000", set.obterEndereco());
		set.clicarProsseguir();
	}

	@E("^aceito os termos de serviço e prossigo$")
	public void aceitoOsTermosDeServiçoEProssigo() throws Throwable {
		entrega.clicarCheckBox();
		entrega.clicarProssigaCheckout();
	}

	@E("^valido o valor total da compra$")
	public void validoOValorTotalDaCompra() throws Throwable {
		Assert.assertEquals("$30.16", mostra.obterTotal());
	}

	@E("^seleciono um método de pagamento e prossigo$")
	public void selecionoUmMétodoDePagamentoEProssigo() throws Throwable {
		mostra.selecionarPagamento();
	}

	@Então("^confirmo a compra e valido se foi finalizada com sucesso$")
	public void confirmoACompraEValidoSeFoiFinalizadaComSucesso() throws Throwable {
		mostra.confirmaOrdem();
		mostra.obterMensagem();
	}

	@After(order = 0)
	public void finaliza() {
		quitDriver();
	}
	
	@After(order = 1)
	public void print(Scenario scenario) throws IOException{
		
		String testName = scenario.getName();
		
		screenShot(testName);
	}
	
	
}
