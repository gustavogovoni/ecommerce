package br.com.brq.ecommerce.pages;

import br.com.brq.ecommerce.helper.Esperas;
import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class PagamentoPage {

	SeleniumFunctions page = new SeleniumFunctions();
	Esperas wait = new Esperas();

	public void selecionarPagamento() {
		wait.esperaImplicita();
		page.clicarBotao("//*[@class='bankwire']");
	}

	public String obterTotal() {
		return page.obterTexto("//*[@id='total_price']");
	}

	public void confirmaOrdem() {
		page.clicarBotao("//*[@class='button btn btn-default button-medium']");
	}

	public String obterMensagem() {
		wait.esperaImplicita();
		return page.obterTexto("//*[@class='cheque-indent']/strong[@class='dark']");
	}

}
