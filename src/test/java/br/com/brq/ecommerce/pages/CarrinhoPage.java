package br.com.brq.ecommerce.pages;

import br.com.brq.ecommerce.helper.Esperas;
import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class CarrinhoPage {

	SeleniumFunctions page = new SeleniumFunctions();
	Esperas wait = new Esperas();

	public void adicionarCarrinho() {
		wait.esperaImplicita();
		page.clicarBotao("//*[@id='add_to_cart']/button[@type='submit']");
	}

	public void prosseguirCheckout() {
		wait.esperaImplicita();
		page.clicarBotao(
				"//a[@class='btn btn-default button button-medium']//span[contains(text(),'Proceed to checkout')]");
	}

}
