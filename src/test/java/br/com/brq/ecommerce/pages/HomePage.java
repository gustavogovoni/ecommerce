package br.com.brq.ecommerce.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import br.com.brq.ecommerce.helper.Esperas;
import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class HomePage extends SeleniumFunctions {

	Esperas wait = new Esperas();

	public void selecionarProduto(String xpath) {
		List<WebElement> elements = getDriver()
				.findElements(By.xpath("//*[@id='homefeatured']//div[@class='right-block']/h5/a"));
		wait.esperaExplicita(elements);
		String produto = getDriver().findElement(By.xpath(xpath)).getText();
		ArrayList<String> titles = new ArrayList<String>();
		int cont = 0;
		for (WebElement element : elements) {
			titles.add(element.getAttribute("title"));
			if (titles.get(cont).equals(produto)) {
				clicarBotao(xpath);
				break;
			}
			scrollObj("//*[@id='homefeatured']//div[@class='right-block']/h5/a");
			cont++;
		}

	}

}
