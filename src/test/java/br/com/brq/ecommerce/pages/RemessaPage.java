package br.com.brq.ecommerce.pages;

import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class RemessaPage {

	SeleniumFunctions page = new SeleniumFunctions();

	public void clicarCheckBox() {
		page.clicarBotao("//*[@id='uniform-cgv']");
	}

	public void clicarProssigaCheckout() {
		page.scrollObj("//*[@class='button btn btn-default standard-checkout button-medium']");
		page.clicarBotao("//*[@class='button btn btn-default standard-checkout button-medium']");

	}

}
