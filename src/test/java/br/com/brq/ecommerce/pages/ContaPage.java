package br.com.brq.ecommerce.pages;

import br.com.brq.ecommerce.helper.JavaFaker;
import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class ContaPage {

	JavaFaker fake = new JavaFaker();
	SeleniumFunctions page = new SeleniumFunctions();
	String email = "";

	public String inserirEmail(boolean exist) {
		if (!exist) {
			email = fake.criarEmail();
			page.escreverTexto("//*[@id='email_create']", email);
		}
		System.out.println("E-mail:" + email);
		return email;
	}

	public void clicarCriarConta() {
		page.clicarBotao("//*[@id='SubmitCreate']");

	}

}
