package br.com.brq.ecommerce.pages;

import br.com.brq.ecommerce.helper.Esperas;
import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class ResumoPage {

	SeleniumFunctions page = new SeleniumFunctions();
	Esperas wait = new Esperas();

	public String validarProdutoCarrinho() {

		return page.obterTexto("//*[@class='cart_description']/p[@class='product-name']");
	}

	public void prosseguirCheckout() {
		wait.esperaImplicita();
		page.clicarBotao("//p[@class='cart_navigation clearfix']//span[contains(text(),'Proceed to checkout')]");
	}

}
