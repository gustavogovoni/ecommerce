package br.com.brq.ecommerce.pages;

import br.com.brq.ecommerce.helper.Esperas;
import br.com.brq.ecommerce.helper.JavaFaker;
import br.com.brq.ecommerce.utils.SeleniumFunctions;

public class EnderecoPage {

	SeleniumFunctions page = new SeleniumFunctions();
	ContaPage conta = new ContaPage();
	JavaFaker faker = new JavaFaker();
	Esperas wait = new Esperas();
	boolean exist = false;
	String nome = "";
	String sonbrenome = "";

	public String insereNome(boolean exist) {
		if (!exist) {
			nome = faker.criarNome();
			wait.esperaImplicita();
			page.scrollObj("//*[@id='customer_firstname']");
			page.escreverTexto("//*[@id='customer_firstname']", nome);
		}
		return nome;

	}

	public String insereSobreNome(boolean exist) {
		if (!exist) {
			sonbrenome = faker.criarSobrenome();
			wait.esperaImplicita();
			page.scrollObj("//*[@id='customer_lastname']");
			page.escreverTexto("//*[@id='customer_lastname']", sonbrenome);
		}
		return sonbrenome;
	}

	public void insereEmail() {
		String email = conta.inserirEmail(true);
		wait.esperaImplicita();
		page.scrollObj("//*[@id='email']");
		page.escreverTexto("//*[@id='email']", email);

	}

	public void insereSenha() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='passwd']");
		page.escreverTexto("//*[@id='passwd']", "12345");

	}

	public void insereEndereco() {
		String endereco = faker.criarEndereco();
		wait.esperaImplicita();
		page.scrollObj("//*[@id='address1']");
		page.escreverTexto("//*[@id='address1']", endereco);
	}

	public void insereCidade() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='city']");
		page.escreverTexto("//*[@id='city']", "Chicago");
	}

	public void selecionaEstado() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='id_state']");
		page.selecionarCombo("//*[@id='id_state']", "Alabama");

	}

	public void insereCodPostal() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='postcode']");
		page.escreverTexto("//*[@id='postcode']", "00000");
	}

	public void selecionaPais() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='id_country']");
		page.selecionarCombo("//*[@id='id_country']", "United States");

	}

	public void insereCelular() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='phone_mobile']");
		page.escreverTexto("//*[@id='phone_mobile']", "(51)99999-4444");
	}

	public void insereReferencia() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='alias']");
		page.escreverTexto("//*[@id='alias']", "My address");
	}

	public void clicarRegistrar() {
		wait.esperaImplicita();
		page.scrollObj("//*[@id='submitAccount']");
		page.clicarBotao("//*[@id='submitAccount']");
	}

	public String obterEndereco() {
		return page.obterTexto(
				"//*[@id='address_delivery']//li[@class='address_city address_state_name address_postcode']");

	}

	public void clicarProsseguir() {
		wait.esperaImplicita();
		page.scrollObj("//*[@name='processAddress']");
		page.clicarBotao("//*[@name='processAddress']");
	}

}
